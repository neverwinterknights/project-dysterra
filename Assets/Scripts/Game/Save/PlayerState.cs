﻿using System;
using Game.Base;
using Game.Helper;

namespace Game.Save
{
    /// <summary>
    /// Defines the player's state. Saved to disk and loaded at game start.
    /// </summary>
    [Serializable]
    public class PlayerState
    {
        /// <summary>
        /// The current scene we're in.
        /// </summary>
        public int CurrentSceneId;

        /// <summary>
        /// The player's current transform (or end transform if the player quits during a move)
        /// </summary>
        public SaveableVector3 CurrentTransform;

        /// <summary>
        /// The player's current scale (or end scale if the player quits during a move)
        /// </summary>
        public SaveableVector3 CurrentScale;

        /// <summary>
        /// The target direction (currently not used).
        /// </summary>
        public Direction CurrentFace;

        /// <summary>
        /// Updates the values above from <see cref="GameState.Instance"/>
        /// </summary>
        public void Update()
        {
            CurrentSceneId = GameState.Instance.LastSceneLoaded;
            CurrentTransform = new SaveableVector3(GameState.Instance.InstancePlayer.TargetPosition);
            CurrentScale = new SaveableVector3(GameState.Instance.InstancePlayer.TargetScale);
            CurrentFace = GameState.Instance.InstancePlayer.TargetFace;
        }
    }
}