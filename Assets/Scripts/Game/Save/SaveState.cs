﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Game.Base;
using Game.Helper;
using UnityEngine;

namespace Game.Save
{
    /// <summary>
    /// Manages game saves/loads.
    /// </summary>
    public class SaveState
    {
        /// <summary>
        /// The game's current <see cref="Game.Save.ScriptState"/>
        /// </summary>
        public static ScriptState ScriptState = new ScriptState();

        /// <summary>
        /// The game's current <see cref="Game.Save.PlayerState"/>
        /// </summary>
        public static PlayerState PlayerState = new PlayerState();

        /// <summary>
        /// Updates <see cref="PlayerState"/> and saves both that and <see cref="ScriptState"/> to disk.
        /// </summary>
        public static void Save()
        {
            SaveScriptState();
            PlayerState.Update();
            SavePlayerState();

            GameState.Instance.InstancePlayer.Remark("Game Saved");
        }

        /// <summary>
        /// Loads <see cref="PlayerState"/> and <see cref="ScriptState"/> from disk.
        /// </summary>
        /// <returns><c>true</c> if both succeeded, <c>false</c> otherwise.</returns>
        public static bool Load()
        {
            bool ss = LoadScriptState();
            bool ps = LoadPlayerState();

            return ss && ps;
        }

        /// <summary>
        /// Saves <see cref="ScriptState"/> to disk, overwriting anything else there.
        /// </summary>
        public static void SaveScriptState()
        {
            //TODO: Corruption checks and backups
            BinaryFormatter bf = new BinaryFormatter();
            FileStream save = File.Create(Application.persistentDataPath + Constants.ScriptStateSaveFileName);
            bf.Serialize(save, ScriptState);
            save.Close();
        }

        /// <summary>
        /// Loads <see cref="ScriptState"/> from the disk.
        /// </summary>
        /// <returns></returns>
        public static bool LoadScriptState()
        {
            //TODO: Corruption checks and backups
            if (File.Exists(Application.persistentDataPath + Constants.ScriptStateSaveFileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream save = File.Open(
                    Application.persistentDataPath + Constants.ScriptStateSaveFileName,
                    FileMode.Open);
                ScriptState = (ScriptState) bf.Deserialize(save);
                save.Close();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Saves <see cref="PlayerState"/> to disk.
        /// </summary>
        public static void SavePlayerState()
        {
            //TODO: Corruption checks and backups
            BinaryFormatter xs = new BinaryFormatter();
            FileStream save = File.Create(Application.persistentDataPath + Constants.PlayerStateSaveFileName);
            xs.Serialize(save, PlayerState);
            save.Close();
        }

        /// <summary>
        /// Loads <see cref="PlayerState"/> from disk.
        /// </summary>
        /// <returns></returns>
        public static bool LoadPlayerState()
        {
            //TODO: Corruption checks and backups
            if (File.Exists(Application.persistentDataPath + Constants.PlayerStateSaveFileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream save = File.Open(
                    Application.persistentDataPath + Constants.PlayerStateSaveFileName,
                    FileMode.Open);
                PlayerState = (PlayerState) bf.Deserialize(save);
                save.Close();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Clears the disk saves for both <see cref="PlayerState"/> and <see cref="ScriptState"/>.
        /// </summary>
        public static void ClearSaves()
        {
            File.Delete(Application.persistentDataPath + Constants.PlayerStateSaveFileName);
            File.Delete(Application.persistentDataPath + Constants.ScriptStateSaveFileName);
            
            if (GameState.Instance != null)
            {
                GameState.Instance.InstancePlayer.Remark("Game Cleared!", Color.black, 2F);
            }
        }
    }
}