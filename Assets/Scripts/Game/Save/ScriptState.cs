﻿using System;
using System.Collections.Generic;
using Game.Interaction;
using UnityEngine;
using ScriptSwitch = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, bool>>;

namespace Game.Save
{
    /// <summary>
    /// Represents the state of the script. Includes all the switches.
    /// </summary>
    [Serializable]
    public class ScriptState
    {
        /// <summary>
        /// The database of script switches.
        /// </summary>
        private ScriptSwitch _scriptSwitches;

        public ScriptState()
        {
            _scriptSwitches = new ScriptSwitch();
        }

        /// <summary>
        /// Gets the state of a switch within the ScriptSwitches database
        /// </summary>
        /// <param name="actionable">Actionable to retrieve stuff from</param>
        /// <param name="key">The switch to retrieve</param>
        /// <returns>The state of the switch. (also <c>false</c> on error) </returns>
        public bool Get(Actionable actionable, string key)
        {
            string actionableId = actionable.ID;
            if (_scriptSwitches.ContainsKey(actionableId))
            {
                if (_scriptSwitches[actionableId].ContainsKey(key))
                {
                    return _scriptSwitches[actionableId][key];
                }
                Debug.LogError("Actionable " + actionableId + " tried to get a key " + key + " that isn't registered!");
            }
            else
            {
                Debug.LogError("Actionable " + actionableId + " is not registered!");
            }
            return false;
        }

        /// <summary>
        /// Sets the state of a switch within the ScriptSwitches databse.
        /// </summary>
        /// <param name="actionable">Actionable whose switches will be modified.</param>
        /// <param name="key">Key to modify</param>
        /// <param name="val">Value to change it to.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        public bool Set(Actionable actionable, string key, bool val)
        {
            string actionableId = actionable.ID;
            if (_scriptSwitches.ContainsKey(actionableId))
            {
                if (_scriptSwitches[actionableId].ContainsKey(key))
                {
                    _scriptSwitches[actionableId][key] = val;
                    return true;
                }
                Debug.LogError("Actionable " + actionableId + " tried to set a key " + key + " that isn't registered!");
            }
            else
            {
                Debug.LogError("Actionable " + actionableId + " is not registered!");
            }
            return false;
        }

        /// <summary>
        /// Registers an Actionable with the database.
        /// </summary>
        /// <param name="actionable">The Actionable to register.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        public bool RegisterActionable(Actionable actionable)
        {
            if (_scriptSwitches.ContainsKey(actionable.ID))
            {
                Debug.LogError(
                    "Actionable " + actionable.ID + " tried to re-register itself! Are there duplicate names?");
                return false;
            }
            _scriptSwitches.Add(actionable.ID, new Dictionary<string, bool>());
            return true;
        }

        /// <summary>
        /// Registers a key with the ScriptSwitches database.
        /// </summary>
        /// <remarks>
        /// Default value is false.
        /// </remarks>
        /// <param name="actionable">Actionable to add to</param>
        /// <param name="key">key to register</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        public bool RegisterKey(Actionable actionable, string key)
        {
            string actionableId = actionable.ID;
            if (_scriptSwitches.ContainsKey(actionableId))
            {
                if (_scriptSwitches.ContainsKey(key))
                {
                    Debug.LogError("Actionable " + actionableId + " tried to register a key that already exists!");
                    return false;
                }
                _scriptSwitches[actionableId].Add(key, false);
                return true;
            }
            Debug.LogError("Actionable " + actionableId + " is not registered!");
            return false;
        }

        /// <summary>
        /// Gets if an actionable is registered.
        /// </summary>
        /// <param name="actionable">Actionable to check.</param>
        /// <returns><c>true</c> if registered, <c>false</c> otherwise. </returns>
        public bool IsRegistered(Actionable actionable)
        {
            return _scriptSwitches.ContainsKey(actionable.ID);
        }
    }
}