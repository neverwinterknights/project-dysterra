﻿using Game.Base;
using Game.Save;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Test
{
    public class BaseSpawn : MonoBehaviour
    {
        private int _spawnId;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (FindObjectsOfType<GameState>().Length < 1)
            {
                _spawnId = SceneManager.GetActiveScene().buildIndex;
                Destroy(FindObjectOfType<Room>().gameObject);
                SaveState.ClearSaves();
                SaveState.PlayerState.CurrentSceneId = _spawnId;
                SceneManager.sceneLoaded += SpawnBase;
                SceneManager.LoadScene(0, LoadSceneMode.Single);
            }
            Destroy(gameObject);
        }

        private void SpawnBase(Scene scene, LoadSceneMode lsm)
        {
            if (lsm == LoadSceneMode.Single)
            {
                GameState.Instance.LoadNewRoom(_spawnId, "");
                SceneManager.sceneLoaded -= SpawnBase;
            }
        }
    }
}