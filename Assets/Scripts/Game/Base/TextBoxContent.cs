﻿using UnityEngine;

namespace Game.Base
{
    /// <summary>
    /// Representation of the content of a text box.
    /// </summary>
    /// <remarks>
    /// A TextBoxContent should have EITHER a <see cref="Transition"/> XOR a <see cref="Choice"/> -- that is to say,
    /// <see cref="HasChoice"/> and <see cref="HasTransition"/> should never both be true.
    /// </remarks>
    public class TextBoxContent
    {
        /// <summary>
        /// The text this box is intially populated with, before it's altered by choices.
        /// </summary>
        private readonly string[] _text;
        
        /// <summary>
        /// Gets the <see cref="Game.Base.Choice"/> associated with this text box.
        /// </summary>
        /// <remarks>
        /// Does no <c>null</c> checking.
        /// </remarks>
        public Choice Choice { get; private set; }
        
        /// <summary>
        /// Gets the <see cref="Transition"/> associated with this text box.
        /// </summary>
        public Transition Transition { get; private set; }
        
        /// <summary>
        /// Determines if there is a <see cref="Base.Choice"/> associated with this TextBoxContent
        /// </summary>
        public bool HasChoice
        {
            get { return Choice != null; }
        }

        /// <summary>
        /// Determines if there is a <see cref="Base.Transition"/> associated with this TextBoxContent
        /// </summary>
        public bool HasTransition
        {
            get { return Transition != null; }
        }

        /// <summary>
        /// Simple text-only TextBoxContent constructor.
        /// </summary>
        /// <param name="text"></param>
        public TextBoxContent(string[] text) : this(text, null, null) { }

        /// <summary>
        /// Simple constructor if this TextBoxContent has a choice at the end of it.
        /// </summary>
        /// <param name="text"><see cref="_text"/></param>
        /// <param name="choice"><see cref="Choice"/></param>
        public TextBoxContent(string[] text, Choice choice) : this(text, null, choice) { }

        /// <summary>
        /// Simple constructor if this TextBoxContent has a transition only at the end of it.
        /// </summary>
        /// <param name="text"><see cref="_text"/></param>
        /// <param name="transition"><see cref="Transition"/></param>
        public TextBoxContent(string[] text, Transition transition) : this(text, transition, null) { }

        /// <summary>
        /// Private constructor that is delegated to by all the others.
        /// </summary>
        /// <param name="text"><see cref="_text"/></param>
        /// <param name="transition"><see cref="Transition"/></param>
        /// <param name="choice"><see cref="Choice"/></param>
        private TextBoxContent(string[] text, Transition transition, Choice choice)
        {
            _text = text;
            Transition = transition;
            Choice = choice;
            if (Transition != null && Choice != null)
                Debug.LogError("Invalid TextBoxContent created!!");
        }

        /// <summary>
        /// Gets the text at <c>index</c>.
        /// </summary>
        /// <param name="index">The index to get text at.</param>
        /// <returns>The text at <see cref="_text"/>[index]</returns>
        public string GetText(int index)
        {
            return _text[index];
        }

        /// <summary>
        /// Determines if there are more boxes after the current one.
        /// </summary>
        /// <param name="currentIndex">The current index.</param>
        /// <returns>
        /// <c>true</c> if there's more to go. \n
        /// <c>false</c> if we're done.
        /// </returns>
        public bool HasNext(int currentIndex)
        {
            return currentIndex + 1 < _text.Length;
        }


        
    }
}