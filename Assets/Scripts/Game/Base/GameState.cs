﻿using System.Collections.Generic;
using Game.Base.Cam;
using Game.Delegates;
using Game.Helper;
using Game.Identifiers;
using Game.Interaction;
using Game.Save;
using Game.Test;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Base
{
    /// <summary>
    /// A class that holds the game's state. Attatched to an empty <see cref="GameObject"/> in the scene.
    /// </summary>
    public class GameState : MonoBehaviour
    {
        private static GameState _cachedInstance;

        /// <summary>
        /// Gets the one-and-only GameState.
        /// </summary>
        /// <remarks>
        /// This is necessary because you can only set values in Unity if they're public and non-static.
        /// </remarks>
        public static GameState Instance
        {
            get { return _cachedInstance ?? (_cachedInstance = FindObjectOfType<GameState>()); }
        }

        /// <summary>
        /// The prefab for the <see cref="Remark"/> class.
        /// </summary>
        public GameObject RemarkObject;

        /// <summary>
        /// The prefab that has the loading screen.
        /// </summary>
        public GameObject LoadingScreenObject;

        /// <summary>
        /// The prefab that contains a generic tooltip.
        /// </summary>
        public GameObject TooltipObject;

        /// <summary>
        /// The canvas on which tooltips are placed.
        /// </summary>
        public GameObject MouseHoverCanvas;

        /// <summary>
        /// Gives the game's <see cref="TextBox"/> instance.
        /// </summary>
        /// <seealso cref="TextBox"/>
        public TextBox InstanceTextBox;

        /// <summary>
        /// Gives the game's question box instance.
        /// </summary>
        public GameObject InstanceQuestionBox;

        /// <summary>
        /// Gives the Choice boxes.
        /// </summary>
        public GameObject[] InstanceChoiceBoxes;

        /// <summary>
        /// The Player
        /// </summary>
        /// <seealso cref="Player"/>
        public Player InstancePlayer;

        /// <summary>
        /// Used to "turn off" one or both of the sprites for the overlay.
        /// </summary>
        /// <seealso cref="TextBox.UpdateSprite"/>
        public Sprite TransparentSprite;

        /// <summary>
        /// The canvas that houses the player menu.
        /// </summary>
        public GameObject PlayerMenuCanvas;

        /// <summary>
        /// Contains the "talk" voice audio blip.
        /// </summary>
        public AudioSource TalkSfxAudioSource;

        /// <summary>
        /// Contains the music currently playing.
        /// </summary>
        public AudioSource MusicAudioSource;

        /// <summary>
        /// Queues callbacks to be run after the current action (<see cref="Action"/>) is finished.
        /// </summary>
        private List<SimpleActionCallback> _queuedCallbacks = new List<SimpleActionCallback>(1);

        /// <summary>
        /// Stores the current <see cref="Actionable"/> that is being run.
        /// </summary>
        /// <seealso cref="HasAction"/>
        public Actionable Action { get; private set; }

        /// <summary>
        /// Determines if <see cref="Action"/> is <c>null</c> or not. If it is, then there's no action happening right now.
        /// </summary>
        /// <returns>
        /// <c>true</c> if an action exists (if <see cref="Action"/> is not <c>null</c>) \n
        /// <c>false</c> if an action doesn't exist (if <see cref="Action"/> is <c>null</c>)
        /// </returns>
        public bool HasAction
        {
            get { return Action != null; }
        }

        /// <summary>
        /// Represents the frozen state of the game.
        /// </summary>
        /// <seealso cref="Freeze"/>
        private bool _isFrozen;

        /// <summary>
        /// Stores the last needed entry ID.
        /// </summary>
        private string _lastEntryId;

        /// <summary>
        /// Stores the ID of the last scene loaded.
        /// </summary>
        public int LastSceneLoaded { get; private set; }

        /// <summary>
        /// Temporary bool for scene loading; Scene ID if we should load the player's position/scale from save for that scene;
        /// -1 otherwise.
        /// </summary>
        private int _loadPlayerTransformFromSave = -1;

        /// <summary>
        /// <c>true</c> if we should autosave, <c>false</c> otherwise.
        /// </summary>
        private bool _autosave = true;

        /// <summary>
        /// Game initialization will go here.
        /// </summary>
        void Awake()
        {
            DontDestroyOnLoad(this);
            SceneManager.sceneLoaded += FinishLoad;
            SaveState.LoadScriptState();

            BaseSpawn baseSpawner = FindObjectOfType<BaseSpawn>();
            if (baseSpawner != null)
            {
                return;
            }

            if (SaveState.LoadPlayerState() && SaveState.PlayerState.CurrentSceneId > 0)
            {
                _loadPlayerTransformFromSave = SaveState.PlayerState.CurrentSceneId;
                LoadNewRoom(SaveState.PlayerState.CurrentSceneId, "");
            }
            else
            {
                LoadNewRoom(1, ""); //TODO: Ensure that Build Index 1 is the TBC Title Screen!
            }
        }

        /// <summary>
        /// Freezes the game state by setting <see cref="Time.timeScale"/> to 0.
        /// </summary>
        /// <remarks>
        /// Currently unused. I also think there may be a better way to freeze everything, since setting
        /// <c>Time.timeScale</c> to 0 stops more than just movement, and doesn't stop things like click events.
        /// </remarks>
        public void Freeze()
        {
            Time.timeScale = 0;
        }


        /// <summary>
        /// Updates <see cref="Action"/> only if it is <c>null</c>
        /// </summary>
        /// <param name="a">The <see cref="Actionable"/> to conditionally replace <see cref="Action"/></param>
        /// <returns>
        /// <c>true</c> upon success \n
        /// <c>false</c> otherwise
        /// </returns>
        /// <seealso cref="Action"/>
        /// <seealso cref="HasAction"/>
        /// <seealso cref="Actionable"/>
        public bool UpdateAction(Actionable a)
        {
            if (HasAction)
            {
                Debug.Log("Tried to update an action with an actionable already in progress!");
                Debug.Log(Action);
                return false;
            }

            Action = a;
            return true;
        }

        /// <summary>
        /// Sets <see cref="Action"/> to <c>null</c>
        /// </summary>
        /// <remarks>
        /// There should probably be a condition on this. Just in case.
        /// </remarks>
        public void ClearAction()
        {
            //TODO: Add conditional so that this can't be called if there's actually an action in progress??
            Action = null;
            RunQueuedCallbacks();
        }

        /// <summary>
        /// Called by <see cref="ClearAction"/> -- runs queued callbacks.
        /// </summary>
        /// <seealso cref="ClearAction"/>
        /// <seealso cref="AddQueuedCallback"/>
        private void RunQueuedCallbacks()
        {
            foreach (SimpleActionCallback cb in _queuedCallbacks)
            {
                cb();
            }
            _queuedCallbacks.Clear();
        }

        /// <summary>
        /// Adds a queued callback if there's an action going on right now. Otherwise executes the callback right away.
        /// </summary>
        /// <param name="cb"></param>
        /// <returns></returns>
        public void AddQueuedCallback(SimpleActionCallback cb)
        {
            if (!HasAction) cb();
            else _queuedCallbacks.Add(cb);
        }

        /// <summary>
        /// Loads a new room
        /// </summary>
        /// <param name="sceneId">Scene Build ID to load</param>
        /// <param name="targetId">The target enterance the Player should go to</param>
        public void LoadNewRoom(int sceneId, string targetId)
        {
            LastSceneLoaded = sceneId;
            _lastEntryId = targetId;
            Room toDestroy = FindObjectOfType<Room>();
            if (toDestroy != null)
            {
                Fader2D[] faders = toDestroy.GetComponentsInChildren<Fader2D>();
                foreach (Fader2D fader in faders)
                {
                    fader.FadeOut();
                }
                InstancePlayer.GetComponent<Fader2D>().FadeOut();
                FaderAudio[] audioFaders = toDestroy.GetComponentsInChildren<FaderAudio>();
                foreach (FaderAudio fader in audioFaders)
                {
                    StartCoroutine(fader.FadeOut());
                }
                if (faders.Length > 0 && faders[faders.Length - 1] != null)
                {
                    faders[faders.Length - 1].FadeOutComplete += delegate
                    {
                        Destroy(toDestroy.gameObject);
                        Instantiate(LoadingScreenObject, Camera.main.transform);
                        SceneManager.LoadSceneAsync(sceneId, LoadSceneMode.Additive);
                    };
                    return;
                }
                Destroy(toDestroy.gameObject);
            }
            else Debug.Log("No Room found! (Normal on first room load)");
            Instantiate(LoadingScreenObject, Camera.main.transform);
            SceneManager.LoadSceneAsync(sceneId, LoadSceneMode.Additive);
        }

        /// <summary>
        /// Puts the Player at the location of the targeted enterance, and fades in new GameObjects.
        /// </summary>
        /// <param name="scene">The scene that is being loaded</param>
        /// <param name="lsm">The manner in which the said scene was loaded</param>
        public void FinishLoad(Scene scene, LoadSceneMode lsm)
        {
            if (_loadPlayerTransformFromSave == scene.buildIndex)
            {
                InstancePlayer.transform.position = SaveState.PlayerState.CurrentTransform;
                InstancePlayer.transform.localScale = SaveState.PlayerState.CurrentScale;
                InstancePlayer.CurrentFace = SaveState.PlayerState.CurrentFace;
                InstancePlayer.SetTargetTransform(
                    SaveState.PlayerState.CurrentTransform,
                    SaveState.PlayerState.CurrentFace);
                _loadPlayerTransformFromSave = -1;
            }
            else
            {
                if (FindObjectsOfType<EntryPoint>().Length > 0)
                {
                    EntryPoint target = null;
                    foreach (EntryPoint ep in FindObjectsOfType<EntryPoint>())
                    {
                        if (ep.ID == _lastEntryId) target = ep;
                    }
                    if (target == null) target = FindObjectsOfType<EntryPoint>()[0];

                    Vector3 targetTransform = target.transform.position + new Vector3(0, 0, -0.001f) +
                                              target.TargetPositionOffset;
                    
                    InstancePlayer.Flip(target.StartDirection);
                    InstancePlayer.transform.position = targetTransform;
                    InstancePlayer.transform.localScale = InstancePlayer.DoScaling(targetTransform);
                    FindObjectOfType<PlayerCam>().Snap();
                }
            }
            GameObject roomBase = GameObject.Find(scene.name);
            if (roomBase != null)
            {
                Fader2D[] faders = roomBase.GetComponentsInChildren<Fader2D>();
                foreach (Fader2D fader in faders)
                {
                    fader.FadeIn();
                }
                InstancePlayer.GetComponent<Fader2D>().FadeIn();
                InstancePlayer.GetComponent<Fader2D>().FadeInComplete += delegate
                {
                    foreach (LoadingScreen ls in FindObjectsOfType<LoadingScreen>())
                    {
                        FaderCanvas f = ls.GetComponentInChildren<FaderCanvas>();
                        f.FadeOutComplete += delegate { Destroy(ls.gameObject); };
                        f.FadeOut();
                    }
                };
                FaderAudio[] audioFaders = roomBase.GetComponentsInChildren<FaderAudio>();
                foreach (FaderAudio fader in audioFaders)
                {
                    StartCoroutine(fader.FadeIn());
                }
                Room rm = roomBase.GetComponent<Room>();
                if (rm != null)
                {
                    if (rm.RoomMusic != null && MusicAudioSource.clip != roomBase.GetComponent<Room>().RoomMusic)
                    {
                        StartCoroutine(
                            MusicAudioSource.GetComponent<FaderAudio>()
                                .FadeTo(roomBase.GetComponent<Room>().RoomMusic));
                    }
                    else if (rm.RoomMusic == null)
                    {
                        StartCoroutine(MusicAudioSource.GetComponent<FaderAudio>().FadeOut());
                    }
                }
            }
        }

        /// <summary>
        /// Saves the game on quitting the application.
        /// </summary>
        private void OnApplicationQuit()
        {
            if (_autosave)
                SaveState.Save();
        }

// ============= TEMPORARY, until we have a menu ===============
        /// <summary>
        /// Canvas w/ debug menu
        /// </summary>
        public GameObject SpecCanvas;

        /// <summary>
        /// Opens the debug menu if Right Alt is held down.
        /// </summary>
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.RightAlt))
            {
                SpecCanvas.SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.RightAlt))
            {
                SpecCanvas.SetActive(false);
            }
        }

        /// <summary>
        /// Does different save actions. Totally janky and lame, but works for debugging!
        /// </summary>
        /// <param name="action">The action number.</param>
        public void SaveAction(int action)
        {
            switch (action)
            {
                case 0:
                    SaveState.Save();
                    break;
                case 1:
                    SaveState.Load();
                    break;
                case 2:
                    SaveState.LoadPlayerState();
                    break;
                case 3:
                    SaveState.LoadScriptState();
                    break;
                case 4:
                    SaveState.ClearSaves();
                    break;
                case 5:
                    ToggleAutosave();
                    break;
            }
        }

        /// <summary>
        /// Toggles autosave, notifies the developer.
        /// </summary>
        public void ToggleAutosave()
        {
            if (_autosave)
            {
                InstancePlayer.Remark("Autosave Disabled!", Color.black, 2F);
            }
            else
            {
                InstancePlayer.Remark("Autosave Enabled!", Color.black, 2F);
            }
            _autosave = !_autosave;
        }

        /// <summary>
        /// Quits the game.
        /// </summary>
        public void Quit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}