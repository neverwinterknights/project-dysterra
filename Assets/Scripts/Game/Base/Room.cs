﻿using System;
using Game.Interaction;
using UnityEngine;

namespace Game.Base
{
    /// <summary>
    /// Represents properties of the current room. Applied to a parent object to all the room's objects.
    /// </summary>
    /// <remarks>
    /// Really, this should be changed to be its own class and a property of a room object. I don't really know
    /// how I would do "room detection" though.
    /// </remarks>
    public class Room : MonoBehaviour
    {
        /// <summary>
        /// The music that should play in this room.
        /// </summary>
        public AudioClip RoomMusic;

        /// <summary>
        /// Sets this object not to destroy itself on a load, so that it can be referenced across scenes.
        /// </summary>
        private void Start()
        {
            DontDestroyOnLoad(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newYPos"></param>
        /// <returns></returns>
        public float DoScaling(float newYPos)
        {
            Debug.Log("Scale In => " + newYPos);
            Yardstick[] yardsticks = FindObjectsOfType<Yardstick>();
            if (yardsticks.Length == 1)
            {
                return yardsticks[0].GetComponent<Collider2D>().bounds.size.y;
            }
            if (yardsticks.Length != 2)
            {
                Debug.LogError("Invalid scaling in Room " + gameObject.name + ". Scaling Disabled!");
                return 1f;
            }
            Transform ysFarTf;
            Transform ysNearTf;
            float ysFarHeight;
            float ysNearHeight;
            if (yardsticks[0].transform.position.y > yardsticks[1].transform.position.y)
            {
                ysFarTf = yardsticks[0].transform;
                ysNearTf = yardsticks[1].transform;
                ysFarHeight = yardsticks[0].GetComponent<Collider2D>().bounds.size.y;
                ysNearHeight = yardsticks[1].GetComponent<Collider2D>().bounds.size.y;
            }
            else if (Math.Abs(yardsticks[0].transform.position.y - yardsticks[1].transform.position.y) < 0.001f)
            {
                Debug.LogError("Invalid scaling in Room " + gameObject.name + ". Scaling Disabled!");
                return 1f;
            }
            else
            {
                ysFarTf = yardsticks[1].transform;
                ysNearTf = yardsticks[0].transform;
                ysFarHeight = yardsticks[1].GetComponent<Collider2D>().bounds.size.y;
                ysNearHeight = yardsticks[0].GetComponent<Collider2D>().bounds.size.y;
            }

            float scaleOut = ysNearHeight
                   * Mathf.Pow(
                       Mathf.Pow(
                           ysFarHeight / ysNearHeight,
                           1f / (ysFarTf.position.y - ysNearTf.position.y)),
                       newYPos - ysNearTf.position.y);

            Debug.Log("Scale Out <= " + scaleOut);
            return scaleOut;
        }
    }
}