﻿using Game.Helper;
using Game.Interaction;
using UnityEngine;

namespace Game.Base
{
    /// <summary>
    /// Represents a transition between text box.
    /// </summary>
    public class Transition
    {
        /// <summary>
        /// The left-side character
        /// </summary>
        public Actionable LeftCharacter { get; private set; }

        /// <summary>
        /// The right-side character
        /// </summary>
        public Actionable RightCharacter { get; private set; }

        /// <summary>
        /// How <see cref="TextBox"/> should update the sprites.
        /// </summary>
        public SpriteTransitionType TransitionType { get; private set; }

        /// <summary>
        /// Gets the associated <see cref="TextBoxContent"/>.
        /// </summary>
        /// <remarks>
        /// Does no <c>null</c> checking.
        /// </remarks>
        public TextBoxContent Content { get; private set; }

        /// <summary>
        /// Basic constructor with no sprite transition.
        /// </summary>
        /// <param name="newContent">The content</param>
        public Transition(TextBoxContent newContent) : this(
            SpriteTransitionType.Update,
            null,
            null,
            newContent) { }

        /// <summary>
        /// Basic (but complete) constructor.
        /// </summary>
        /// <param name="leftCharacter"><see cref="LeftCharacter"/></param>
        /// <param name="rightCharacter"><see cref="RightCharacter"/></param>
        /// <param name="newContent"><see cref="Content"/></param>
        /// <param name="stt">Should the new content overwrite the previous sprites?</param>
        /// <seealso cref="TextBoxContent"/>
        public Transition(
            SpriteTransitionType stt,
            Actionable leftCharacter,
            Actionable rightCharacter,
            TextBoxContent newContent)
        {
            LeftCharacter = leftCharacter;
            RightCharacter = rightCharacter;
            Content = newContent;
            TransitionType = stt;
        }
    }
}