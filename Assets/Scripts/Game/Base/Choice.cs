﻿using Game.Delegates;

namespace Game.Base
{
    /// <summary>
    /// Representation of a dialogue choice.
    /// </summary>
    /// <remarks>
    ///
    /// </remarks>
    public class Choice
    {
        /// <summary>
        /// An array of the choice number to the set of result boxes.
        /// </summary>
        /// <remarks>
        /// (choice option : int) -> (result transition : <see cref="Transition"/>)
        /// </remarks>
        private Transition[] _transitions;

        /// <summary>
        /// An array of choice numbers to options.
        /// </summary>
        /// <remarks>
        /// (choice option : int) -> (choice : string)
        /// </remarks>
        private string[] _choices;

        /// <summary>
        /// The question being asked.
        /// </summary>
        private string _question;

        /// <summary>
        /// An array of callbacks, so that choices can do more than just text boxes.
        /// </summary>
        /// <remarks>
        /// (choice option : int) -> (callback : <see cref="SimpleActionCallback"/>
        /// </remarks>
        private SimpleActionCallback[] _callbacks;

        /// <summary>
        /// Constructor for a choice. Very simple input-to-private-fields.
        /// </summary>
        /// <param name="question">
        ///     See <see cref="_question"/>
        /// </param>
        /// <param name="choices">
        ///     See <see cref="_choices"/>
        /// </param>
        /// <param name="transitions">
        ///     See <see cref="_transitions"/>
        /// </param>
        /// <param name="callbacks">A list of optional callbacks</param>
        public Choice(
            string question,
            string[] choices,
            Transition[] transitions,
            SimpleActionCallback[] callbacks = null)
        {
            _transitions = transitions;
            _choices = choices;
            _callbacks = callbacks;
            _question = question;
        }

        /// <summary>
        /// Gets the number of choices
        /// </summary>
        /// <returns>The number of choices, 0-3</returns>
        public int GetNumberOfChoices()
        {
            return _choices.Length;
        }

        /// <summary>
        /// Gets the choices
        /// </summary>
        /// <returns>A string array of choices.</returns>
        public string[] GetChoices()
        {
            return _choices;
        }

        /// <summary>
        /// Gets the result for the selected index.
        /// </summary>
        /// <param name="index">The choice selected, 0-2</param>
        /// <returns>The <see cref="Transition"/> associated with said choice.</returns>
        public Transition GetChoiceResult(int index)
        {
            return _transitions[index];
        }

        /// <summary>
        /// Determines if the given choice has a callback.
        /// </summary>
        /// <param name="index">The index to check.</param>
        /// <returns><c>true</c> if it has a callback, <c>false</c> otherwise.</returns>
        public bool HasCallback(int index)
        {
            return _callbacks != null && index < _callbacks.Length && _callbacks[index] != null;
        }

        /// <summary>
        /// Gets the callback for the given index.
        /// </summary>
        /// <param name="index">Index to get</param>
        /// <returns>The attatched <see cref="SimpleActionCallback"/>.</returns>
        /// <remarks>
        /// Does no null/length checking.
        /// </remarks>
        public SimpleActionCallback GetCallback(int index)
        {
            return _callbacks[index];
        }

        /// <summary>
        /// Gets the question text.
        /// </summary>
        /// <returns>The string of the question.</returns>
        public string GetQuestion()
        {
            return _question;
        }
    }
}