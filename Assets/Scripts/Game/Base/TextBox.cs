﻿using System;
using Game.Helper;
using Game.Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Base
{
    /// <summary>
    /// Represents the text box.
    /// </summary>
    /// <remarks>
    /// There's only one textbox.
    /// </remarks>
    public class TextBox : MonoBehaviour
    {
        /// <summary>
        /// The <see cref="RectTransform"/> attribute of the arrow, used to move it up and down.
        /// </summary>
        public RectTransform ArrowTransform;

        /// <summary>
        /// The <see cref="Canvas"/> containing the textbox and character sprites.
        /// </summary>
        public GameObject OverlayCanvas;

        /// <summary>
        /// The image representations of the sprites shown on either side.
        /// </summary>
        public Image LeftSprite;

        /// <inheritdoc cref="LeftSprite"/>
        public Image RightSprite;

        /// <summary>
        /// The Actionable representations of the sprites shown on either side.
        /// </summary>
        private Actionable _leftActionable;

        /// <inheritdoc cref="_leftActionable"/>
        private Actionable _rightActionable;

        /// <summary>
        /// The actual <see cref="Text"/> of the text box.
        /// </summary>
        public Text TextArea;

        /// <summary>
        /// Keeps track of the time the last character appeared when writing out text.
        /// </summary>
        private float _lastCharacterAppearedTime;

        /// <summary>
        /// The content of this textbox.
        /// </summary>
        /// <seealso cref="TextBoxContent"/>
        private TextBoxContent _currentContent;

        /// <summary>
        /// <c>true</c> if there's more pages of text to go.
        /// <c>false</c> otherwise.
        /// </summary>
        private bool _hasContinue;

        /// <summary>
        /// The current page of text we're on.
        /// </summary>
        private int _pageIndex;

        /// <summary>
        /// The text of the current page.
        /// </summary>
        private string _pageText;

        /// <summary>
        /// The current string index, used when writing out text.
        /// </summary>
        private int _pageTextIndex;

        /// <summary>
        /// The max string index, used when writing out text.
        /// </summary>
        private int _maxPageTextIndex;

        /// <summary>
        /// The top of the arrow's transform arc.
        /// </summary>
        private Vector3 _arrowStartTransform;

        /// <summary>
        /// The bottom of the arrow's transform arc.
        /// </summary>
        private Vector3 _arrowEndTransform;

        /// <summary>
        /// The percent through the arrow's transform arc, oscillates between 1 and 0.
        /// </summary>
        private float _arrowFloatPercent = 1f;

        /// <summary>
        /// The current direction the arrow is moving in.
        /// </summary>
        private Direction _arrowDirection = Direction.Down;

        /// <summary>
        /// The current question being asked.
        /// </summary>
        private string _questionText;

        /// <summary>
        /// The current character we're on for <see cref="_questionText"/>.
        /// </summary>
        private int _questionTextIndex;

        /// <summary>
        /// Same as <see cref="_lastCharacterAppearedTime"/>, but for <see cref="_questionText"/>
        /// </summary>
        private float _lastQuestionCharacterAppearedTime;

        /// <summary>
        /// <c>true</c> if we're currently writing out the question.
        /// </summary>
        private bool _doQuestionWrite;

        /// <summary>
        /// <c>true</c> if we want to accept clicks on the dialogue (does not apply to choices!)
        /// </summary>
        private bool _doTextBoxClick = true;

        /// <summary>
        /// <c>true</c> if we should call <see cref="GameState.ClearAction"/> on finish.
        /// </summary>
        private bool _clearActionOnFinish = true;

        /// <summary>
        /// Used to pause for some amount of time when we run into punctuation.
        /// </summary>
        private int _charsToWait = 0;

        /// <summary>
        /// Initializes the text box. Sets the canvas overlay off, and sets the transform positions.
        /// </summary>
        /// <remarks>
        /// The arrow's start position in the editor is its top position.
        /// </remarks>
        void Start()
        {
            OverlayCanvas.SetActive(false);
            _arrowStartTransform = ArrowTransform.position;
            _arrowEndTransform = ArrowTransform.position - new Vector3(0, 10, 0);
        }

        /// <summary>
        /// Updates per frame. Responsible for doing the text writing.
        /// </summary>
        void Update()
        {
            if (_charsToWait != 0 && Time.time - _lastCharacterAppearedTime > Constants.TextBoxSpeed)
            {
                _charsToWait--;
                _lastCharacterAppearedTime = Time.time;
            }
            if (_pageText != null
                && _pageTextIndex < _maxPageTextIndex
                && Time.time - _lastCharacterAppearedTime > Constants.TextBoxSpeed
                && _charsToWait == 0)
            {
                TextArea.text = _pageText.Substring(0, _pageTextIndex);
                GameState.Instance.TalkSfxAudioSource.Play();
                _pageTextIndex++;
                _lastCharacterAppearedTime = Time.time;
                if (_maxPageTextIndex - _pageTextIndex > 1 && _pageTextIndex > 1)
                    switch (_pageText.Substring(_pageTextIndex - 2, 1))
                    {
                        case ".":
                            _charsToWait = Constants.WaitCharsPeriod;
                            break;
                        case ",":
                            _charsToWait = Constants.WaitCharsComma;
                            break;
                        case "!":
                            _charsToWait = Constants.WaitCharsExclamationMark;
                            break;
                        case "?":
                            _charsToWait = Constants.WaitCharsQuestionMark;
                            break;
                        case ":":
                            _charsToWait = Constants.WaitCharsColon;
                            break;
                        case ";":
                            _charsToWait = Constants.WaitCharsSemicolon;
                            break;
                        case "\v":
                            _charsToWait = Constants.WaitCharsVerticalTab;
                            break;
                    }
            }

            if (_doQuestionWrite
                && _questionText != ""
                && _questionTextIndex <= _questionText.Length
                && Time.time - _lastQuestionCharacterAppearedTime > Constants.TextBoxSpeed)
            {
                GameState.Instance.InstanceQuestionBox.GetComponentInChildren<Text>().text =
                    _questionText.Substring(0, _questionTextIndex);
                _questionTextIndex++;
                _lastQuestionCharacterAppearedTime = Time.time;

                if (_questionTextIndex == Mathf.FloorToInt(_questionText.Length * 0.7f))
                {
                    foreach (GameObject go in GameState.Instance.InstanceChoiceBoxes)
                    {
                        if (go.activeInHierarchy)
                            go.GetComponent<Animator>().SetTrigger("Fade In");
                    }
                }
            }
            else if (_doQuestionWrite && _questionTextIndex >= _questionText.Length)
            {
                _doQuestionWrite = false;
            }
        }

        /// <summary>
        /// Called when the mouse clicks the text box.
        /// </summary>
        /// <remarks>
        /// If the user clicks the box before it's done being written out, it writes it all out instantly.
        /// If the text box is done, it moves on to the next one. If there's no more, it resets the text box and
        /// closes the <see cref="OverlayCanvas"/>
        /// </remarks>
        public void OnMouseDown()
        {
            if (_doTextBoxClick)
            {
                if (_pageText != null && _pageTextIndex != _maxPageTextIndex)
                {
                    _pageTextIndex = _maxPageTextIndex;
                    TextArea.text = _pageText;
                }
                else if (_pageText != null && _pageTextIndex == _maxPageTextIndex)
                {
                    if (!GetNextBox())
                    {
                        ResetBox();
                    }
                }
            }
        }

        /// <summary>
        /// Responsible for controlling arrow movement. Called at a fixed interval.
        /// </summary>
        void FixedUpdate()
        {
            if (_hasContinue)
            {
                DoArrowMovement();
            }
        }

        /// <summary>
        /// Does the actual arrow movement.
        /// </summary>
        private void DoArrowMovement()
        {
            ArrowTransform.position = Vector3.Slerp(
                _arrowStartTransform,
                _arrowEndTransform,
                Mathf.SmoothStep(0, 1, _arrowFloatPercent));
            switch (_arrowDirection)
            {
                case Direction.Up:
                    _arrowFloatPercent += 0.01f;
                    break;
                case Direction.Down:
                    _arrowFloatPercent -= 0.01f;
                    break;
            }
            if (_arrowFloatPercent <= 0 && _arrowDirection == Direction.Down)
            {
                _arrowDirection = Direction.Up;
            }
            else if (_arrowFloatPercent >= 1 && _arrowDirection == Direction.Up)
            {
                _arrowDirection = Direction.Down;
            }
        }

        /// <summary>
        /// Gets the next box, resetting the required variables to do so.
        /// </summary>
        /// <returns>
        /// <c>true</c> to continue.
        /// <c>false</c> if the box should reset, and closes the box if <see cref="_clearActionOnFinish"/>.
        /// </returns>
        public bool GetNextBox()
        {
            if (!_currentContent.HasNext(_pageIndex) && !_currentContent.HasChoice && !_currentContent.HasTransition)
            {
                return false;
            }
            if (!_currentContent.HasNext(_pageIndex) && _currentContent.HasChoice)
            {
                GetChoices();
            }
            if (!_currentContent.HasNext(_pageIndex) && _currentContent.HasTransition)
            {
                // TODO: No choice, but a transition - will this work?
                SetupTextBox(_currentContent.Transition);
            }
            _pageIndex++;
            _hasContinue = _currentContent.HasNext(_pageIndex)
                           || _currentContent.HasChoice
                           || _currentContent.HasTransition;
            if (_currentContent.HasNext(_pageIndex - 1))
            {
                _pageText = _currentContent.GetText(_pageIndex);
                _pageTextIndex = 0;
                _maxPageTextIndex = _pageText.Length + 1;
            }
            if (!_hasContinue)
            {
                ArrowTransform.GetComponent<Image>().color = Color.gray;
            }
            return true;
        }

        /// <summary>
        /// Sets up all the Choices.
        /// </summary>
        public void GetChoices()
        {
            GameState.Instance.InstanceQuestionBox.GetComponentInChildren<Text>().text =
                _currentContent.Choice.GetQuestion();
            GameState.Instance.InstanceQuestionBox.SetActive(true);

            _questionText = _currentContent.Choice.GetQuestion();
            _questionTextIndex = 0;
            _doQuestionWrite = true;
            _doTextBoxClick = false;

            string[] choiceContent = _currentContent.Choice.GetChoices();
            GameObject[] choiceBoxes = GameState.Instance.InstanceChoiceBoxes;
            TextArea.text = "";
            _pageTextIndex = 0;
            _maxPageTextIndex = 0;
            for (int i = 0; i < _currentContent.Choice.GetNumberOfChoices(); i++)
            {
                Text thisText = choiceBoxes[i].GetComponentInChildren<Text>();
                thisText.text = choiceContent[i];
                choiceBoxes[i].SetActive(true);
                choiceBoxes[i].GetComponent<Button>().enabled = true;
                DoChoiceBoxScale(
                    _currentContent.Choice.GetNumberOfChoices(),
                    choiceBoxes[i].GetComponent<RectTransform>(),
                    i);
            }
        }

        /// <summary>
        /// Scales the choice boxes.
        /// </summary>
        /// <param name="max">The number of choice boxes total</param>
        /// <param name="box">The RectTransform of this box</param>
        /// <param name="num">Which box this one is.</param>
        private void DoChoiceBoxScale(int max, RectTransform box, int num)
        {
            Vector2 sizeParent = box.parent.GetComponent<RectTransform>().sizeDelta;
            switch (max)
            {
                case 1:
                    box.sizeDelta = new Vector2(sizeParent.x, sizeParent.y / 2);
                    box.anchoredPosition = new Vector2(sizeParent.x / 4, sizeParent.y / 4);
                    break;
                case 2:
                    box.sizeDelta = new Vector2(sizeParent.x / 2 - 3, sizeParent.y / 2);
                    box.anchoredPosition = new Vector2(
                        (num + 1) * sizeParent.x / 2 - sizeParent.x / 4,
                        sizeParent.y / 4);
                    break;
                case 3:
                    box.sizeDelta = new Vector2(sizeParent.x / 3 - 3, sizeParent.y / 2);
                    box.anchoredPosition = new Vector2(
                        (num + 1) * sizeParent.x / 3 - sizeParent.x / 6,
                        sizeParent.y / 4);
                    break;
            }
        }

        /// <summary>
        /// Activates the TextBox with new <see cref="TextBoxContent"/>.
        /// </summary>
        /// <param name="t">The new content to load in.</param>
        /// <param name="clearActionOnFinish">
        /// Determines whether <see cref="GameState.ClearAction"/>
        /// will be called upon resetting the text box.
        /// </param>
        /// <seealso cref="TextBoxContent"/>
        public void SetupTextBox(Transition t, bool clearActionOnFinish = true)
        {
            _clearActionOnFinish = clearActionOnFinish;
            OverlayCanvas.SetActive(true);
            _currentContent = t.Content;
            UpdateSprite(t.LeftCharacter, t.RightCharacter, t.TransitionType);
            _pageIndex = -1;
            GetNextBox();
        }

        /// <summary>
        /// Recieves a choice from the button
        /// </summary>
        /// <param name="buttonNumber">The button number</param>
        public void DoChoice(int buttonNumber)
        {
            _doTextBoxClick = true;
            ArrowTransform.gameObject.SetActive(true);

            if (_currentContent.Choice.HasCallback(buttonNumber))
            {
                _currentContent.Choice.GetCallback(buttonNumber)();
            }

            Transition t = _currentContent.Choice.GetChoiceResult(buttonNumber);
            if (t != null)
                SetupTextBox(t);

            foreach (GameObject choiceBox in GameState.Instance.InstanceChoiceBoxes)
            {
                if (choiceBox.activeInHierarchy)
                {
                    choiceBox.GetComponent<Animator>().SetTrigger("Fade Out");
                    choiceBox.GetComponent<Button>().enabled = false;
                }
            }
            GameState.Instance.InstanceQuestionBox.SetActive(false);
            if (t == null)
            {
                ResetBox();
            }
        }

        /// <summary>
        /// Updates the sprite in the specified <see cref="Direction"/>
        /// </summary>
        /// <param name="left">The Actionable to update the right to.</param>
        /// <param name="right">The Actionable to update the right to.</param>
        /// <param name="transitionType">Determines how sprites are updated and how <c>null</c> is handled.</param>
        private void UpdateSprite(
            Actionable left = null,
            Actionable right = null,
            SpriteTransitionType transitionType = SpriteTransitionType.Update)
        {
            if (left != null)
            {
                _leftActionable = left;
                LeftSprite.sprite = left.Sprite;
            }
            else if (transitionType == SpriteTransitionType.Overwrite)
            {
                _leftActionable = null;
                LeftSprite.sprite = GameState.Instance.TransparentSprite;
            }

            if (right != null)
            {
                _rightActionable = right;
                RightSprite.sprite = right.Sprite;
            }
            else if (transitionType == SpriteTransitionType.Overwrite)
            {
                _rightActionable = null;
                RightSprite.sprite = GameState.Instance.TransparentSprite;
            }

            DoSpriteScaling();
        }

        /// <summary>
        /// Scales the sprites shown on the left and right.
        /// </summary>
        private void DoSpriteScaling()
        { //TODO: Allow sprites to be scaled even if one is null!
            if (_leftActionable == null && _rightActionable == null) return; // Do nothing if both are null!

            RectTransform canvas = GameState.Instance.InstanceTextBox.transform.parent.GetComponent<RectTransform>();

            
            float scalingFactor;

            if (_leftActionable == null || _rightActionable == null)
            {
                scalingFactor = 1;
            }
            else
            {
                scalingFactor = _leftActionable.Height / _rightActionable.Height;
            }
            
            RectTransform rtr = RightSprite.GetComponent<RectTransform>();
            RectTransform rtl = LeftSprite.GetComponent<RectTransform>();

            if (scalingFactor > 1)
            {
                scalingFactor = Mathf.Pow(scalingFactor, -1);
                rtr.sizeDelta = new Vector2(rtr.sizeDelta.x, canvas.sizeDelta.y * scalingFactor);
                rtl.sizeDelta = new Vector2(rtl.sizeDelta.x, canvas.sizeDelta.y);
            }
            else
            {
                rtl.sizeDelta = new Vector2(rtl.sizeDelta.x, canvas.sizeDelta.y * scalingFactor);
                rtr.sizeDelta = new Vector2(rtl.sizeDelta.x, canvas.sizeDelta.y);
            }

            if (Math.Abs(rtr.eulerAngles.y - 180) < 0.01)
                rtr.Rotate(new Vector3(0, -180, 0));
            if (Math.Abs(rtl.eulerAngles.y) < 0.01)
                rtl.Rotate(new Vector3(0, -180, 0));

            if (_rightActionable != null && _rightActionable.SpriteFace == Direction.Right)
                rtr.Rotate(new Vector3(0, 180, 0));
            if (_leftActionable != null && _leftActionable.SpriteFace == Direction.Left)
                rtl.Rotate(new Vector3(0, 180, 0));
        }

        /// <summary>
        /// Resets the box and turns off the <see cref="OverlayCanvas"/> if <see cref="_clearActionOnFinish"/>.
        /// </summary>
        private void ResetBox()
        {
            _currentContent = null;
            _hasContinue = false;
            _pageIndex = 0;
            _pageText = null;
            _pageTextIndex = 0;
            _arrowFloatPercent = 1f;
            _arrowDirection = Direction.Down;

            // Resets fade in/out stuff -- required if the user clicks through the box faster than the transition.
            foreach (GameObject go in GameState.Instance.InstanceChoiceBoxes)
            {
                if (go.activeInHierarchy)
                {
                    Color color = go.GetComponent<Image>().color;
                    go.GetComponent<Image>().color = new Color(color.r, color.g, color.b, 0);
                    color = go.GetComponentInChildren<Text>().color;
                    go.GetComponentInChildren<Text>().color = new Color(color.r, color.g, color.b, 0);
                    go.GetComponent<Animator>().ResetTrigger("Fade In");
                    go.GetComponent<Animator>().ResetTrigger("Fade Out");
                    go.GetComponent<Button>().enabled = false;
                    go.SetActive(false);
                }
            }

            LeftSprite.sprite = GameState.Instance.TransparentSprite;
            RightSprite.sprite = GameState.Instance.TransparentSprite;
            OverlayCanvas.SetActive(false);
            ArrowTransform.GetComponent<Image>().color = Color.white;
            if (_clearActionOnFinish)
                GameState.Instance.ClearAction();
        }
    }
}