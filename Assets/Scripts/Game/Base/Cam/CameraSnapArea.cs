﻿using System;
using UnityEngine;

namespace Game.Base.Cam
{
    public class CameraSnapArea : MonoBehaviour
    {
        /// <summary>
        /// Optionally set in the Editor.
        /// </summary>
        public float OrthographicSize;
        
        /// <summary>
        /// Gets the orthographic size to fit this collider.
        /// </summary>
        /// <returns>A size.</returns>
        public float Size
        {
            get { return Math.Abs(OrthographicSize) < 0.1f ? transform.localScale.x / 3f : OrthographicSize; }
        }
        
        /// <summary>
        /// Makes the PlayerCam lock to this.
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetInstanceID() == GameState.Instance.InstancePlayer.gameObject.GetInstanceID())
                FindObjectOfType<PlayerCam>().Lock(this);
        }

        /// <summary>
        /// Makes the PlayerCam begin following the player again.
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.GetInstanceID() == GameState.Instance.InstancePlayer.gameObject.GetInstanceID())
                FindObjectOfType<PlayerCam>().StartFollowing();
        }
        

        private void OnDestroy()
        {
            if (FindObjectOfType<PlayerCam>() != null)
                FindObjectOfType<PlayerCam>().StartFollowing();
        }
    }
}