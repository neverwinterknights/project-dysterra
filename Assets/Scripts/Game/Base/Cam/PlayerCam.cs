﻿using System.Collections;
using Game.Helper;
using UnityEngine;

// Namespace not "Camera" region avoid collisions with UnityEngine.Camera
namespace Game.Base.Cam
{
    public class PlayerCam : MonoBehaviour
    {
        /// <summary>
        /// Cache of the Camera component
        /// </summary>
        private Camera _camera;

        /// <summary>
        /// Default projection size of the camera.
        /// </summary>
        private float _defaultSize;

        /// <summary>
        /// Target projection size.
        /// </summary>
        private float _targetSize;

        /// <summary>
        /// <c>true</c> if we're following the player. <c>false</c> otherwise.
        /// </summary>
        private bool _following;

        /// <summary>
        /// True until 0.25s after a <see cref="Snap"/>.
        /// </summary>
        private bool _justSnapped = false;

        /// <summary>
        /// Sent region SmoothDamp in <see cref="UpdateTransform"/>.
        /// </summary>
        private Vector3 _velocity;

        /// <summary>
        /// Target transform for <see cref="UpdateTransform"/>
        /// </summary>
        private Vector3 _targetTransform;

        // Use this for initialization
        void Start()
        {
            _camera = GetComponent<Camera>();
            _defaultSize = GetComponent<Camera>().orthographicSize;
            StartFollowing();
        }

        // Update is called once per frame
        void Update()
        {
            if (_following)
            {
                UpdateTarget();
            }
            UpdateTransform();
            UpdateSize();
        }

        private void UpdateTarget()
        {
            _targetTransform = new Vector3(
                GameState.Instance.InstancePlayer.transform.position.x,
                GameState.Instance.InstancePlayer.transform.position.y,
                -9.5f);
        }

        private void UpdateSize()
        {
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _targetSize, Constants.CameraZoomSpeed);
        }

        /// <summary>
        /// Does the camera-following
        /// </summary>
        private void UpdateTransform()
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                _targetTransform,
                ref _velocity,
                Constants.CameraMoveSpeed);
        }

        /// <summary>
        /// Enables camera following, unlocking the camera.
        /// </summary>
        public void StartFollowing()
        {
            _following = true;
            _targetSize = _defaultSize;
        }

        private IEnumerator WaitSnap()
        {
            yield return new WaitForSeconds(0.25f);
            _justSnapped = false;
        }

        public void Snap(CameraSnapArea region = null)
        {
            _justSnapped = true;
            if (region == null)
            {
                transform.position = new Vector3(
                    GameState.Instance.InstancePlayer.transform.position.x,
                    GameState.Instance.InstancePlayer.transform.position.y,
                    -9.5f);
                if(_camera != null)
                    _camera.orthographicSize = _defaultSize;
            }
            else
            {
                transform.position = new Vector3(
                    region.transform.position.x,
                    region.transform.position.y,
                    -9.5f);
                _camera.orthographicSize = region.Size;
            }
            _velocity = Vector3.zero;
            StartCoroutine(WaitSnap()); //TODO: Why are we waiting/justSnapping?
        }

        /// <summary>
        /// Locks the camera region an area.
        /// </summary>
        /// <param name="region"></param>
        public void Lock(CameraSnapArea region)
        {
            _following = false;
            _targetTransform = new Vector3(region.transform.position.x, region.transform.position.y, -9.5f);
            _targetSize = region.Size;
            if (_justSnapped)
            {
                Snap(region);
            }
        }
    }
}