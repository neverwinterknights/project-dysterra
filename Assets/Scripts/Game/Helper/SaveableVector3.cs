﻿using System;
using UnityEngine;

namespace Game.Helper
{
    /// <summary>
    /// Serializable version of a Vector3
    /// </summary>
    [Serializable]
    public class SaveableVector3
    {
        public float X;
        public float Y;
        public float Z;

        public SaveableVector3()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }

        /// <summary>
        /// Tries to parse a SaveableVector3 out of a string (same format as <see cref="ToString"/>).
        /// </summary>
        /// <param name="s">The string to parse</param>
        /// <remarks>
        /// Doesn't do any parse checking. /shrug.
        /// </remarks>
        public SaveableVector3(string s)
        {
            string[] xyz = s.Split('/');
            X = float.Parse(xyz[0]); // TODO: Check all this so we don't get errors.
            Y = float.Parse(xyz[1]);
            Z = float.Parse(xyz[2]);
        }

        /// <summary>
        /// Copies a Vector3 to a SaveableVector3
        /// </summary>
        /// <param name="copy">The Vector3 to copy.</param>
        public SaveableVector3(Vector3 copy)
        {
            X = copy.x;
            Y = copy.y;
            Z = copy.z;
        }

        /// <summary>
        /// Implicitly changes any given SaveableVector3 to a Unity Vector3
        /// </summary>
        /// <param name="v">The SaveableVector3 to convert.</param>
        /// <returns>A Vector3 equivilant of v.</returns>
        public static implicit operator Vector3(SaveableVector3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

        /// <summary>
        /// Outputs a string of the format X/Y/Z
        /// </summary>
        /// <returns>A string representation of the SaveableVector3.</returns>
        public override string ToString()
        {
            return X + "/" + Y + "/" + Z;
        }
    }
}