﻿using Game.Delegates;
using UnityEngine;

namespace Game.Helper
{
    /// <summary>
    /// Allows the attatched gameobject to fade with the rest of the scene.
    /// </summary>
    public class FaderCanvas : MonoBehaviour
    {
        /// <summary>
        /// Calls stuff when the fade out is complete.
        /// </summary>
        public event SimpleActionCallback FadeOutComplete;

        /// <summary>
        /// Calls stuff when the fade in is complete.
        /// </summary>
        public event SimpleActionCallback FadeInComplete;

        /// <summary>
        /// The attatched SpriteRenderer.
        /// </summary>
        private CanvasGroup _cg;

        /// <summary>
        /// <c>true</c> if we're fading out. Mutually exclusive to <see cref="_fadingIn"/>.
        /// </summary>
        private bool _fadingOut;

        /// <summary>
        /// <c>true</c> if we're fading in. Mutually exclusive to <see cref="_fadingOut"/>
        /// </summary>
        private bool _fadingIn;

        /// <summary>
        /// The time we started fading.
        /// </summary>
        private float _lerpTimeStart;

        /// <summary>
        /// Sets <see cref="_cg"/>.
        /// </summary>
        void Start()
        {
            _cg = GetComponent<CanvasGroup>();
        }

        /// <summary>
        /// Does the fading.
        /// </summary>
        void Update()
        {
            if (_fadingOut)
            {
                float deltaTime = Time.time - _lerpTimeStart;
                float percentComplete = deltaTime / Constants.LoadFadeDuration;

                _cg.alpha = Mathf.Lerp(1, 0, percentComplete);

                if (percentComplete >= 1.0f)
                {
                    _fadingOut = false;
                    OnFadeOutComplete();
                }
            }
            if (_fadingIn)
            {
                float deltaTime = Time.time - _lerpTimeStart;
                float percentComplete = deltaTime / Constants.LoadFadeDuration;

                _cg.alpha = Mathf.Lerp(0, 1, percentComplete);

                if (percentComplete >= 1.0f)
                {
                    _fadingIn = false;
                    OnFadeInComplete();
                }
            }
        }

        /// <summary>
        /// Sets the object to fade out.
        /// </summary>
        public void FadeOut()
        {
            if (!_fadingIn)
            {
                _lerpTimeStart = Time.time;
                _fadingOut = true;
            }
            else
            {
                Debug.Log("Tried to fade out while fading in!");
            }
        }

        /// <summary>
        /// Sets the object to fade in.
        /// </summary>
        public void FadeIn()
        {
            if (!_fadingOut)
            {
                _lerpTimeStart = Time.time;
                _fadingIn = true;
            }
            else
            {
                Debug.Log("Tried to fade in while fading out!");
            }
        }

        /// <summary>
        /// Calls all the handled events when the fade out is complete.
        /// </summary>
        /// <seealso cref="FadeOutComplete"/>
        protected virtual void OnFadeOutComplete()
        {
            var handler = FadeOutComplete;
            if (handler != null) handler();
        }

        /// <summary>
        /// Calls all the handled events when the fade in is complete.
        /// </summary>
        /// <seealso cref="FadeInComplete"/>
        protected virtual void OnFadeInComplete()
        {
            var handler = FadeInComplete;
            if (handler != null) handler();
        }
    }
}