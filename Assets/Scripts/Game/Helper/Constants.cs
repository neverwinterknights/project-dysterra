﻿using Game.Base.Cam;
using Game.Save;

namespace Game.Helper
{
    /// <summary>
    /// A simple container for program-wide constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The time between each character appearing in a text box, in seconds.
        /// </summary>
        public const float TextBoxSpeed = 0.01f;

        /// <summary>
        /// The duration to fade between loading. Adds additional time to level changes!
        /// </summary>
        public const float LoadFadeDuration = 0.2f;

        /// <summary>
        /// Filename for the Script State Save
        /// </summary>
        /// <seealso cref="SaveState"/>
        /// <seealso cref="ScriptState"/>
        public const string ScriptStateSaveFileName = "/save.script.dtsave";

        /// <summary>
        /// Filename for the Player State Save
        /// </summary>
        /// <seealso cref="PlayerState"/>
        /// <seealso cref="SaveState"/>
        public const string PlayerStateSaveFileName = "/save.player.dtsave";

        /// <summary>
        /// "Stiffness" of camera movement. Lower is faster to correct, higher is slower.
        /// </summary>
        /// <seealso cref="PlayerCam"/>
        public const float CameraMoveSpeed = 0.5f;

        /// <summary>
        /// Speed of camera zoom. Lower is slower, higher is faster.
        /// </summary>
        public const float CameraZoomSpeed = 0.05f;

        /// <summary>
        /// Basically, waits the equivalent of <c>value</c> characters each time one of these characters are encountered
        /// in a text box.
        /// </summary>
        public const int WaitCharsPeriod = 15;
        public const int WaitCharsExclamationMark = 7;
        public const int WaitCharsQuestionMark = 10;
        public const int WaitCharsComma = 5;
        public const int WaitCharsColon = 7;
        public const int WaitCharsSemicolon = 10;
        public const int WaitCharsVerticalTab = 20;
    }
}