﻿using UnityEngine;

namespace Game.Helper {
    public class DoNotDestroyOnLoad : MonoBehaviour {

        // Use this for initialization
        void Start () {
		    DontDestroyOnLoad(gameObject);
        }
    }
}
