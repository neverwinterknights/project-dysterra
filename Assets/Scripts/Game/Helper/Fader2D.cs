﻿using Game.Delegates;
using UnityEngine;

namespace Game.Helper
{
    /// <summary>
    /// Allows the attatched gameobject to fade with the rest of the scene.
    /// </summary>
    public class Fader2D : MonoBehaviour
    {
        /// <summary>
        /// Calls stuff when the fade out is complete.
        /// </summary>
        public event SimpleActionCallback FadeOutComplete;

        /// <summary>
        /// Calls stuff when the fade in is complete.
        /// </summary>
        public event SimpleActionCallback FadeInComplete;

        /// <summary>
        /// The attatched SpriteRenderer.
        /// </summary>
        private SpriteRenderer _sr;

        /// <summary>
        /// <c>true</c> if we're fading out. Mutually exclusive to <see cref="_fadingIn"/>.
        /// </summary>
        private bool _fadingOut;

        /// <summary>
        /// <c>true</c> if we're fading in. Mutually exclusive to <see cref="_fadingOut"/>
        /// </summary>
        private bool _fadingIn;

        /// <summary>
        /// The time we started fading.
        /// </summary>
        private float _lerpTimeStart;

        /// <summary>
        /// The color when we're faded in (100% alpha)
        /// </summary>
        private Color _in;

        /// <summary>
        /// The "color" when we're faded out (0% alpha)
        /// </summary>
        private Color _out;

        /// <summary>
        /// Sets <see cref="_sr"/>, <see cref="_in"/>, and <see cref="_out"/>.
        /// </summary>
        void Start()
        {
            _sr = GetComponent<SpriteRenderer>();
            _in = new Color(_sr.color.r, _sr.color.g, _sr.color.b, _sr.color.a);
            _out = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 0);
        }

        /// <summary>
        /// Does the fading.
        /// </summary>
        void Update()
        {
            if (_fadingOut)
            {
                float deltaTime = Time.time - _lerpTimeStart;
                float percentComplete = deltaTime / Constants.LoadFadeDuration;

                _sr.color = Color.Lerp(_in, _out, percentComplete);

                if (percentComplete >= 1.0f)
                {
                    _fadingOut = false;
                    OnFadeOutComplete();
                }
            }
            if (_fadingIn)
            {
                float deltaTime = Time.time - _lerpTimeStart;
                float percentComplete = deltaTime / Constants.LoadFadeDuration;

                _sr.color = Color.Lerp(_out, _in, percentComplete);

                if (percentComplete >= 1.0f)
                {
                    _fadingIn = false;
                    OnFadeInComplete();
                }
            }
        }

        /// <summary>
        /// Sets the object to fade out.
        /// </summary>
        public void FadeOut()
        {
            _fadingIn = false;
            _lerpTimeStart = Time.time;
            _fadingOut = true;
        }

        /// <summary>
        /// Sets the object to fade in.
        /// </summary>
        public void FadeIn()
        {
            _fadingOut = false;
            _lerpTimeStart = Time.time;
            _fadingIn = true;
        }

        /// <summary>
        /// Calls all the handled events when the fade out is complete.
        /// </summary>
        /// <seealso cref="FadeOutComplete"/>
        protected virtual void OnFadeOutComplete()
        {
            var handler = FadeOutComplete;
            if (handler != null) handler();
        }

        /// <summary>
        /// Calls all the handled events when the fade in is complete.
        /// </summary>
        /// <seealso cref="FadeInComplete"/>
        protected virtual void OnFadeInComplete()
        {
            var handler = FadeInComplete;
            if (handler != null) handler();
        }
    }
}