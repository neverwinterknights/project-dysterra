﻿using System.Collections;
using UnityEngine;

namespace Game.Helper
{
    public class FaderAudio : MonoBehaviour
    {
        public AudioSource AudioSource;

        private float _targetVol = 1;

        private void Awake()
        {
            _targetVol = AudioSource.volume;
            Debug.Log(name + " => " + _targetVol);
        }

        public IEnumerator FadeOut()
        {
            float startVolume = AudioSource.volume;

            while (AudioSource != null && AudioSource.volume > 0)
            {
                AudioSource.volume -= startVolume * Time.deltaTime / Constants.LoadFadeDuration;

                yield return null;
            }

            if (AudioSource != null)
            {
                AudioSource.Stop();
                AudioSource.volume = startVolume;
            }
        }

        public IEnumerator FadeIn()
        {
            AudioSource.volume = 0;
            AudioSource.Play();

            while (AudioSource.volume < _targetVol)
            {
                AudioSource.volume += _targetVol * Time.deltaTime / Constants.LoadFadeDuration;

                yield return null;
            }
        }

        public IEnumerator FadeTo(AudioClip target)
        {
            yield return StartCoroutine(FadeOut());
            AudioSource.clip = target;
            yield return StartCoroutine(FadeIn());
        }
    }
}