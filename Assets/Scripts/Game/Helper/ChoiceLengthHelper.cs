﻿using UnityEngine;

namespace Game.Helper {
	public class ChoiceLengthHelper : MonoBehaviour
	{

		public RectTransform Rec;
		public RectTransform Parent;

		// Use this for initialization
		private void OnEnable()
		{
			float parentWidth = Parent.rect.width;
			float parentHeight = Parent.rect.height;
			Rec.sizeDelta = new Vector2(Parent.rect.width, Parent.rect.height);
			Rec.anchoredPosition = new Vector2(parentWidth / 2, parentHeight / 2);
		}
	}
}
