﻿using System;

namespace Game.Helper
{
    /// <summary>
    /// Simple enum of directions
    /// </summary>
    [Serializable]
    public enum Direction
    {
        /// <summary>
        /// Facing Left
        /// </summary>
        Left,

        /// <summary>
        /// Facing Right
        /// </summary>
        Right,

        /// <summary>
        /// Going Up
        /// </summary>
        Up,

        /// <summary>
        /// Going Down
        /// </summary>
        Down,
        
        /// <summary>
        /// The lack of directionality
        /// </summary>
        None
    }
}