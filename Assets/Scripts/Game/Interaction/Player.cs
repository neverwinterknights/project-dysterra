﻿using System;
using Game.Base;
using Game.Base.Cam;
using Game.Helper;
using UnityEngine;

namespace Game.Interaction
{
    /// <summary>
    /// Representation of the player character
    /// </summary>
    /// <remarks>
    /// Possibly should inherit <see cref="Actionable"/>.
    /// </remarks>
    /// <seealso cref="Actionable"/>
    public class Player : Entity
    {
        /// <summary>
        /// A CamSnap prefab, used in <see cref="Activate"/>
        /// </summary>
        public GameObject CamSnap;

        /// <summary>
        /// Defines how quickly the character moves. Set in the Unity editor.
        /// </summary>
        public float Speed;

        /// <inheritdoc />
        public override float Height
        {
            get { return transform.localScale.y; }
        }
        
        /// <summary>
        /// Defines the scale after a movement
        /// </summary>
        /// <seealso cref="SetTargetTransform"/>
        /// <seealso cref="FixedUpdate"/>
        public Vector3 TargetScale { get; private set; }

        /// <summary>
        /// Defines if the player character is in the process of moving.
        /// </summary>
        /// <seealso cref="FixedUpdate"/>
        private bool _moving;

        /// <summary>
        /// Defines the movement (lerp) start time.
        /// </summary>
        /// <seealso cref="FixedUpdate"/>
        private float _lerpTimeStart;

        /// <summary>
        /// Defines the starting point of a movement.
        /// </summary>
        /// <seealso cref="SetTargetTransform"/>
        /// <seealso cref="FixedUpdate"/>
        private Vector3 _startTransform;

        /// <summary>
        /// Defines the scale before a movement.
        /// </summary>
        /// <seealso cref="SetTargetTransform"/>
        /// <seealso cref="FixedUpdate"/>
        private Vector3 _startScale;

        /// <summary>
        /// Player initialization
        /// </summary>
        protected void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _startTransform = transform.position;
            TargetPosition = transform.position;
            TargetScale = transform.localScale;
            CurrentFace = Direction.Right;
        }

        /// <summary>
        /// Blank override prevents Actionable.Start()'s actions being called.
        /// </summary>
        protected override void Start() { }

        /// <summary>
        /// Responsible for handling smooth movements.
        /// </summary>
        private void FixedUpdate()
        {
            if (_moving)
            {
                float percentComplete = 1f;
                if (_startTransform != TargetPosition)
                {
                    float deltaTime = Time.time - _lerpTimeStart;
                    percentComplete = deltaTime
                                      / (Speed
                                         * Mathf.Pow((_startTransform - TargetPosition).magnitude / 2f, 1f / 6f));

                    transform.position = Vector3.Lerp(_startTransform, TargetPosition, percentComplete);
                    transform.localScale = Vector3.Lerp(_startScale, TargetScale, percentComplete);
                }
                if (percentComplete >= 1.0f)
                {
                    _moving = false;
                    OnFinishMove();
                }
            }
        }

        /// <summary>
        /// Called upon finishing a move.
        /// </summary>
        /// <remarks>
        /// Currently, activates the <see cref="Actionable"/>'s action, and properly flips the character.
        /// </remarks>
        private void OnFinishMove()
        {
            _moving = false;
            if (CurrentFace != TargetFace) Flip();

            if (GameState.Instance.HasAction)
                GameState.Instance.Action.Activate();
        }

        /// <summary>
        /// Creates a menu.
        /// </summary>
        public override void Activate()
        {
            GameState.Instance.PlayerMenuCanvas.SetActive(true);

            CameraSnapArea[] csas = FindObjectsOfType<CameraSnapArea>(); // Disable other camera snap areas
            foreach (var csa in csas)
            {
                csa.gameObject.SetActive(false);
            }
            
            // Create a new camera snap area to zoom into the menu.
            CameraSnapArea cs = Instantiate(CamSnap, transform.position, Quaternion.identity, transform)
                .GetComponent<CameraSnapArea>();
            cs.OrthographicSize = GameState.Instance.InstancePlayer.Height / 2;
            FindObjectOfType<PlayerCam>().StartFollowing();
            FindObjectOfType<PlayerCam>().Lock(cs);

            GameState.Instance
                     .InstanceTextBox
                     .SetupTextBox(
                         new Transition(
                             SpriteTransitionType.Overwrite,
                             this,
                             null,
                             new TextBoxContent(new[] {"I should focus on my mission right now..."})
                         ),
                         false
                     );

            GameState.Instance
                     .AddQueuedCallback(
                         delegate
                         {
                             foreach (var csa in csas)
                             {
                                 csa.gameObject.SetActive(true);
                             }
                             Destroy(cs); // Re-enable all the diabled camera snap areas.
                             GameState.Instance.PlayerMenuCanvas.SetActive(false);
                         });
        }

        /// <summary>
        /// Flips the character to be facing right or left. 
        /// Sets <see cref="Actionable.CurrentFace"/> appropriately. 
        /// If d is Direction.None, flips the character to face the opposite direction.
        /// </summary>
        /// <seealso cref="Actionable.CurrentFace"/>
        /// <seealso cref="FixedUpdate"/>
        public void Flip(Direction d = Direction.None)
        {
            if (d != CurrentFace)
            {
                transform.localScale = new Vector3(
                    transform.localScale.x * -1,
                    transform.localScale.y,
                    transform.localScale.z);
                TargetScale = new Vector3(TargetScale.x * -1, TargetScale.y, TargetScale.z);
                if (CurrentFace == Direction.Left)
                    CurrentFace = Direction.Right;
                else
                    CurrentFace = Direction.Left;
            }
        }

        /// <summary>
        /// Called when an action begins. Begins movement and scaling.
        /// </summary>
        /// <param name="target">The target position.</param>
        /// <param name="targetFace">The target facing direction.</param>
        /// <seealso cref="ActionEvent"/>
        /// <seealso cref="DoScaling"/>
        /// <seealso cref="Flip"/>
        public void SetTargetTransform(Vector3 target, Direction targetFace)
        {
            _startTransform = transform.position; // Simply initializes variables.
            TargetPosition = target;
            TargetScale = DoScaling(target);
            TargetFace = targetFace;

            // Flips the character for the movement (we don't want him walking backwards!)
            if (target.x - transform.position.x > 0 && CurrentFace == Direction.Left)
            {
                Flip();
            }
            if (target.x - transform.position.x < 0 && CurrentFace == Direction.Right)
            {
                Flip();
            }

            // Sets the scale after we figure out flipping, since flipping is also handled by the scale variable.
            _startScale = transform.localScale;

            // Aaand, we're moving!
            _moving = true;

            // Start the clock.
            _lerpTimeStart = Time.time;
        }

        /// <summary>
        /// Scales the character. Delegates to <see cref="Room.DoScaling"/>
        /// </summary>
        /// <param name="targetPosition">The target position</param>
        /// <returns>A vector representation of the local scale.</returns>
        /// <seealso cref="SetTargetTransform"/>
        public Vector3 DoScaling(Vector3 targetPosition)
        {
            float scale = FindObjectOfType<Room>().DoScaling(targetPosition.y);

            int xScale = CurrentFace == Direction.Right ? -1 : 1;
            return new Vector3(xScale * scale, scale, 0.1f);
        }

        /// <summary>
        /// Recieves requests to begin a new action.
        /// </summary>
        /// <param name="a">The proposed new <see cref="Actionable"/>.</param>
        /// <returns>
        /// <c>true</c> if we succeeded in getting the action. \n
        /// <c>false</c> if we were in the middle of another action already.
        /// </returns>
        /// <seealso cref="GameState.UpdateAction"/>
        /// <seealso cref="Actionable.OnMouseDown"/>
        public bool ActionEvent(Actionable a)
        {
            if (!GameState.Instance.UpdateAction(a)) return false;
            if (a == this)
            {
                Activate();
                return true;
            }
            SetTargetTransform(a.TargetPosition, a.TargetFace);
            return true;
        }
    }
}