﻿using System;
using System.Collections.Generic;
using Game.Base;
using TMPro;
using UnityEngine;

namespace Game.Interaction
{
    /// <summary>
    /// Represents an entity
    /// </summary>
    public abstract class Entity : Actionable
    {
        /// <summary>
        /// List of remarks happening right now. Used to prevent duplicate remarks from occuring concurrently.
        /// </summary>
        private List<string> _currentRemarks = new List<string>();

        /// <summary>
        /// Shows a tiny speech bubble near the player without going into Dialogue Mode.
        /// </summary>
        /// <param name="remark">What to remark.</param>
        /// <param name="startColor"></param>
        /// <param name="duration"></param>
        /// <param name="positionOffset"></param>
        public void Remark(
            string remark,
            Color startColor,
            float duration = 0f,
            Vector3 positionOffset = default(Vector3))
        {
            if (!_currentRemarks.Contains(remark))
            {
                Vector3 startOffset = new Vector3(2.25f, 0.25f, 0);
                if (positionOffset != Vector3.zero)
                {
                    startOffset = positionOffset;
                }

                GameObject rmk = Instantiate(GameState.Instance.RemarkObject);
                rmk.GetComponent<Remark>().Attatched = this;
                rmk.GetComponent<TextMeshPro>().text = remark;
                rmk.GetComponent<Remark>().Offset = startOffset;

                if (startColor != new Color(0, 0, 0, 0))
                {
                    rmk.GetComponent<TextMeshPro>().color = new Color32(
                        (byte) (255 * startColor.r),
                        (byte) (255 * startColor.g),
                        (byte) (255 * startColor.b),
                        255);
                }
                if (Math.Abs(duration) >= 0.0001f)
                {
                    rmk.GetComponent<Remark>().Duration = duration;
                }

                _currentRemarks.Add(remark);
            }
        }

        /// <summary>
        /// Sends a remark with the default color.
        /// </summary>
        /// <param name="remark">The string to remark.</param>
        public void Remark(string remark)
        {
            Remark(remark, new Color(0, 0, 0, 0));
        }

        /// <summary>
        /// Removes the remark from the database once it's done (prevents duplicate remarks happening at once).
        /// </summary>
        /// <param name="remark">The remark to remove.</param>
        public void DoneRemarking(string remark)
        {
            _currentRemarks.Remove(remark);
        }
    }
}