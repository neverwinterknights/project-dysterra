﻿using Game.Helper;
using UnityEngine;

namespace Game.Interaction
{
    /// <summary>
    /// Defines where the player should enter. Typically a door.
    /// </summary>
    /// <remarks>
    /// An object can be both an exit and entrance.
    /// </remarks>
    public class EntryPoint : MonoBehaviour
    {
        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// The ID of this entry point, referred to by <see cref="ExitPoint"/>s.
        /// </summary>
        public string ID;

        /// <summary>
        /// Defines an offset for where the character should appear.
        /// </summary>
        public Vector3 TargetPositionOffset;

        /// <summary>
        /// The direction the player should be facing when entering.
        /// </summary>
        public Direction StartDirection;
    }
}