﻿using TMPro;
using UnityEngine;

namespace Game.Interaction
{
    public class Remark : MonoBehaviour
    {
        public Entity Attatched;

        public TextMeshPro Text;
        public float Duration;
        public Vector3 Offset;

        private float _startTime;
        private float _steadyTime;
        private float _disappearingTime;
        private float _endTime;

        private Color _hidden;
        private Color _normal;

        private Vector3 _start;
        private Vector3 _end;

        // Use this for initialization
        void Start()
        {
            _startTime = Time.time;
            _steadyTime = Time.time + Duration / 5f;
            _disappearingTime = Time.time + Duration / 5f * 4f;
            _endTime = _startTime + Duration;

            _normal = Text.color;
            _hidden = new Color(Text.color.r, Text.color.g, Text.color.b, 0);
            Text.color = _hidden;
        }

        // Update is called once per frame
        void Update()
        {
            UpdatePosition();
            transform.position = Vector3.Lerp(_start, _end, (Time.time - _startTime) / Duration);
            if (Time.time - _steadyTime < 0)
                Text.color = Color.Lerp(_hidden, _normal, 5f * (Time.time - _startTime) / Duration);
            if (Time.time - _disappearingTime > 0)
                Text.color = Color.Lerp(_normal, _hidden, 5f * (Time.time - _disappearingTime) / Duration);
            if (Time.time - _endTime > 0)
                Destroy(gameObject);
        }

        /// <summary>
        /// Updates the position so that the Remark follows the entity. Also destroys the remark if the attatched
        /// gameobject is destroyed.
        /// </summary>
        private void UpdatePosition()
        {
            if (Attatched == null) Destroy(gameObject);
            else
            {
                _start = Attatched.transform.position + Offset;
                _end = _start + new Vector3(0, 0.1f, -0.1f);
            }
        }

        /// <summary>
        /// Marks this remark as done to the attatched GameObject, if it still exists.
        /// </summary>
        private void OnDestroy()
        {
            if (Attatched != null)
            {
                Attatched.DoneRemarking(Text.text);
            }
        }
    }
}