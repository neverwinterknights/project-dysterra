﻿using Game.Base;
using TMPro;
using UnityEngine;

namespace Game.Interaction {
	public class Tooltip : MonoBehaviour
	{

		public string TooltipText;

		private GameObject _tooltipInstance;

		private Vector3 _offset = Vector3.zero;
		
		private void OnMouseEnter()
		{
			if (_tooltipInstance == null && !GameState.Instance.HasAction)
			{
				_tooltipInstance = Instantiate(GameState.Instance.TooltipObject, GameState.Instance.MouseHoverCanvas.transform);

				TextMeshProUGUI text = _tooltipInstance.GetComponent<TextMeshProUGUI>();
				text.text = TooltipText;
				Rect rect = _tooltipInstance.GetComponent<RectTransform>().rect;
				_offset = new Vector3(rect.width * 0.5f, rect.height * 0.5f);
			}
		}

		private void OnMouseExit()
		{
			if (_tooltipInstance != null)
			{
				Destroy(_tooltipInstance);
			}
		}

		private void Update()
		{
			if (_tooltipInstance != null && !GameState.Instance.HasAction)
			{
				_tooltipInstance.transform.position = Input.mousePosition + _offset;
			}
			else if (GameState.Instance.HasAction)
			{
				Destroy(_tooltipInstance);
			}
		}

		private void OnDestroy()
		{
			OnMouseExit();
		}
	}
}
