﻿using Game.Base;
using Game.Helper;
using Game.Save;
using UnityEngine;

namespace Game.Interaction
{
    /// <summary>
    /// Describes an object that is actionable -- that is, the player can click on it to cause an event to happen.
    /// </summary>
    /// <remarks>
    /// This is designed to be used in tandem with other behaviors that are attatched to actionable GameObjects.
    /// </remarks>
    public class Actionable : MonoBehaviour
    {
        /// <summary>
        /// Gets the associated sprite of this Actionable.
        /// </summary>
        /// <returns>The SpriteRenderer's sprite</returns>
        public Sprite Sprite
        {
            get { return GetComponent<SpriteRenderer>().sprite; }
        }

        /// <summary>
        /// Gets the on-screen height of this Actionable.
        /// </summary>
        /// <returns>The SpriteRenderer's bounds' y-size.</returns>
        public virtual float Height
        {
            get { return GetComponent<BoxCollider2D>().bounds.size.y; }
        }

        /// <summary>
        /// Gets the on-screen width of this Actionable.
        /// </summary>
        /// <returns>The SpriteRenderer's bounds' y-size.</returns>
        public virtual float Width
        {
            get { return GetComponent<BoxCollider2D>().bounds.size.x; }
        }

        /// <summary>
        /// Describes where the player should move to.
        /// </summary>
        /// <seealso cref="Start"/>
        public Vector3 TargetPosition { get; protected set; }

        /// <summary>
        /// A user-definable offset for the target position.
        /// </summary>
        public Vector3 TargetPositionOffset;

        /// <summary>
        /// Describes what direction the player should face when interacting with this..
        /// </summary>
        /// <remarks>
        /// Left by default.
        /// </remarks>
        /// <seealso cref="TargetPosition"/>
        public Direction TargetFace;

        /// <summary>
        /// Describes what direction the entity is currently facing.
        /// </summary>
        /// <remarks>
        /// Right by default.
        /// </remarks>
        public virtual Direction CurrentFace
        {
            get;
            set;
        }

        /// <summary>
        /// Set by the Unity editor; determines which way the *origional sprite* is facing.
        /// </summary>
        /// <remarks>
        /// Should not be updated during game time unless the actual sprite changes direction for some reason.
        /// Needed because not all sprites face the same direction.
        /// </remarks>
        public Direction SpriteFace;

        /// <summary>
        /// Unique ID for this Actionable. By default, is the name of the GameObject.
        /// </summary>
        /// <seealso cref="Start"/>
        // ReSharper disable once InconsistentNaming
        public string ID { get; private set; }

        /// <summary>
        /// Called during <see cref="GameObject"/> initialization.
        /// </summary>
        protected virtual void Start()
        {
            TargetPosition = transform.position + new Vector3(0, 0, -0.001f) + TargetPositionOffset;
            CurrentFace = SpriteFace;
            ID = name;
        }

        /// <summary>
        /// Event called when the mouse is clicked over this <c>GameObject</c>.
        /// </summary>
        /// <seealso cref="Player"/>
        /// <seealso cref="Player.ActionEvent"/>
        protected virtual void OnMouseDown()
        {
            GameState.Instance.InstancePlayer.ActionEvent(this);
        }

        /// <summary>
        /// Called when the Actionable's Action is Activated. Default: No Action
        /// </summary>
        public virtual void Activate()
        {
            GameState.Instance.ClearAction();
        }

        /// <summary>
        /// Registers this Actionable with <see cref="SaveState.ScriptState"/>.
        /// Assumes all keys are false by default.
        /// </summary>
        /// <param name="stateKeys">List of keys to add.</param>
        protected virtual void Register(string[] stateKeys)
        {
            if (!SaveState.ScriptState.IsRegistered(this))
            {
                SaveState.ScriptState.RegisterActionable(this);
                foreach (string key in stateKeys)
                {
                    SaveState.ScriptState.RegisterKey(this, key);
                }
            }
        }
    }
}