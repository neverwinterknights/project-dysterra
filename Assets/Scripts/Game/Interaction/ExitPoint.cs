﻿using Game.Base;

namespace Game.Interaction
{
    /// <summary>
    /// An exit point for the room. A loading zone, if you will.
    /// </summary>
    /// <remarks>
    /// An object can be both an exit and entrance.
    /// </remarks>
    public class ExitPoint : Actionable
    {
        /// <summary>
        /// The target scene from the Unity Build Index. Set in the Editor.
        /// </summary>
        public int TargetSceneId;

        /// <summary>
        /// The target Entrance for the exit, in the new scene. Set in the Editor.
        /// </summary>
        public string TargetEntrance;

        /// <summary>
        /// Overrides <see cref="Actionable.Activate"/>. Occurs when the player reaches the Exit Point.
        /// </summary>
        public override void Activate()
        {
            GameState.Instance.LoadNewRoom(TargetSceneId, TargetEntrance);
        }
    }
}