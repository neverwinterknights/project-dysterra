﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Game.Base;
using Game.Helper;
using Game.Interaction;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace GameElements.TestAreaC
{
    public class TrenchMan : Entity
    {
        private string _firstName;

        private void Awake()
        {
            try
            {
                _firstName = Meta.GetRealPlayerName().Split(' ')[0];
            }
            catch (NotImplementedException e)
            {
                Debug.Log(e.Message);
                _firstName = "man";
            }
        }

        public override void Activate()
        {
            // Gets the textbox.
            TextBox tb = GameState.Instance.InstanceTextBox;
            tb.SetupTextBox(
                new Transition(
                    SpriteTransitionType.Overwrite,
                    this,
                    GameState.Instance.InstancePlayer,
                    new TextBoxContent(
                        new[]
                        {
                            "Hey, " + _firstName + ".",
                            "It's been a while.",
                            "Don't worry, I'm not the guy you're looking for. But I do have some information for you.",
                            "Click on yourself to pause the game. You can quit, save, open your inventory, and manage settings from there.",
                            "Good luck."
                        }
                    )
                )
            );
        }
    }
}