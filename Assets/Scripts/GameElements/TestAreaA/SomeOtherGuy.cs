﻿using Game.Base;
using Game.Delegates;
using Game.Helper;
using Game.Interaction;
using Game.Save;
using UnityEngine;

namespace GameElements.TestAreaA
{
    /// <summary>
    /// Some Other Guy.
    /// </summary>
    /// <remarks>
    /// Registers these switches:
    /// punched
    /// left alone
    /// knocked out
    /// asked question
    /// </remarks>
    public class SomeOtherGuy : Entity
    {
        public bool WasPunched
        {
            get { return SaveState.ScriptState.Get(this, "punched"); }
        }

        public bool WasLeftAlone
        {
            get { return SaveState.ScriptState.Get(this, "left alone"); }
        }


        public bool KnockedOut
        {
            get { return SaveState.ScriptState.Get(this, "knocked out"); }
        }

        public bool WasAskedQuestion
        {
            get { return SaveState.ScriptState.Get(this, "asked question"); }
        }

        protected override void Start()
        {
            base.Start();
            Register(
                new[]
                {
                    "punched",
                    "left alone",
                    "knocked out",
                    "asked question"
                });

            // Gets the world size of this sprite.
            Bounds size = GetComponent<SpriteRenderer>().bounds;

            if (KnockedOut) GetComponent<Transform>().Rotate(new Vector3(0, 0, 90));
        }

        //!! Domenic, I'd like you to take a look at this, because this is how we currently do story trees.
        public override void Activate()
        {
            // Gets the textbox.
            TextBox tb = GameState.Instance.InstanceTextBox;

            // Okay, here we go.
            if (WasAskedQuestion)
            {
                GameState.Instance
                         .InstancePlayer
                         .Remark("I don't think I'm gonna be able to get more information out of this guy.");
                GameState.Instance.ClearAction();
            }
            else if (KnockedOut)
            {
                GameState.Instance.InstancePlayer.Remark("He's knocked out cold", Color.red);
                GameState.Instance.ClearAction();
            }
            else if (WasPunched)
            {
                tb.SetupTextBox(
                    new Transition(
                        SpriteTransitionType.Overwrite,
                        this,
                        GameState.Instance.InstancePlayer,
                        new TextBoxContent(
                            new[] {"I don't have anything to say to you."},
                            new Choice(
                                "What do you want to do?",
                                new[] {"Make him have something to say!", "Leave it."},
                                new[]
                                {
                                    new Transition(
                                        new TextBoxContent(new[] {"(you punch him again!)", "HOLY-- WHY?! OW!"})),
                                    new Transition(
                                        new TextBoxContent(
                                            new[] {"(you decide to let him off the hook... this time.)"}))
                                },
                                new SimpleActionCallback[] {Knockout, null})
                        )
                    )
                );
            }
            else
                tb.SetupTextBox(
                    new Transition(
                        SpriteTransitionType.Overwrite,
                        this,
                        GameState.Instance.InstancePlayer,
                        new TextBoxContent(
                            new[] {". . .", ". . .", ". . .", "\". . .\" means \"SCRAM!\" Leave me alone!"},
                            new Choice(
                                "What will you do?",
                                new[] {"Force it out of him!", "Leave him alone", "Talk to him"},
                                new[]
                                {
                                    new Transition(
                                        new TextBoxContent(
                                            new[]
                                            {
                                                "(you punch him)",
                                                "OW! What the heck?! What does a guy need to do to be left alone??"
                                            }
                                        )
                                    ),
                                    new Transition(
                                        new TextBoxContent(
                                            new[]
                                            {
                                                "(you decide to leave well enough alone)"
                                            }
                                        )
                                    ),
                                    new Transition(
                                        new TextBoxContent(
                                            new[]
                                            {
                                                "(you ask him if he's seen a man in a trench coat)",
                                                "Yeah, actually. He went through that door, and he hasn't come back.",
                                                "(you thank him.)",
                                                "Yeah, sure thing.",
                                                "Now... S\vC\vR\vA\vM\v!"
                                            }
                                        )
                                    )
                                },
                                new SimpleActionCallback[]
                                {
                                    Punched,
                                    LeftAlone,
                                    null
                                }
                            )
                        )
                    )
                );
        }

        /// <summary>
        /// Callback for if this character was punched.
        /// </summary>
        protected void Punched()
        {
            if (WasPunched) // This means the guy was punched more than once.
            {
                // Because we want the Remark to appear after the text box stuff is done, we add it as a lambda (anonymous) function and add it to the Callback Queue in GameState.
                GameState.Instance.AddQueuedCallback(delegate { Remark("Again?! What's this guy's problem?"); });
                return;
            }
            if (WasLeftAlone) // This means that you left him alone at one point then punched him.
            {
                SaveState.ScriptState.Set(this, "punched", true);
                SaveState.ScriptState.Set(this, "left alone", false);
                GameState.Instance
                         .AddQueuedCallback(delegate { Remark("I thought he was gonna leave me alone..."); });
                return;
            }
            // Default case
            SaveState.ScriptState.Set(this, "punched", true);
            GameState.Instance.AddQueuedCallback(delegate { Remark("What a jerk..."); });
        }

        public void LeftAlone()
        {
            if (WasPunched) // You punched him before, and are now leaving him alone
            {
                GameState.Instance
                         .AddQueuedCallback(delegate { Remark("Oh man, I thought he was gonna punch me again..."); });
                return;
            }
            if (WasLeftAlone) // You left him alone before and did it again.
            {
                GameState.Instance.AddQueuedCallback(delegate { Remark("???"); });
                return;
            }
            // Default case
            SaveState.ScriptState.Set(this, "left alone", true);
            GameState.Instance.AddQueuedCallback(delegate { Remark("Thank God..."); });
        }

        public void Knockout()
        {
            GameState.Instance
                     .AddQueuedCallback(
                         delegate
                         {
                             FindObjectOfType<SomeOtherGuy>().GetComponent<Transform>().Rotate(new Vector3(0, 0, 90));
                         });
            GameState.Instance
                     .AddQueuedCallback(
                         delegate
                         {
                             GameState
                                 .Instance
                                 .InstancePlayer
                                 .Remark("Serves him right; he shoulda talked the first time.", Color.red);
                         });
            SaveState.ScriptState.Set(this, "knocked out", true);
        }
    }
}