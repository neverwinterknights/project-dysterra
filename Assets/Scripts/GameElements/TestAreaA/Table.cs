﻿using Game.Base;
using Game.Helper;
using Game.Interaction;
using Game.Save;
using UnityEngine;

namespace GameElements.TestAreaA
{
    /// <summary>
    /// A good-looking table
    /// </summary>
    /// <remarks>
    /// Uses this switch:
    /// was talked to
    /// </remarks>
    public class Table : Actionable
    {
        public bool WasTalkedTo
        {
            get { return SaveState.ScriptState.Get(this, "was talked to"); }
        }

        protected override void Start()
        {
            base.Start();
            TargetPosition = new Vector3(
                transform.position.x + transform.lossyScale.x,
                transform.position.y + transform.lossyScale.y / 2,
                transform.position.z - transform.position.z * 0.01f);

            Register(
                new[]
                {
                    "was talked to"
                });
        }

        public override void Activate()
        {
            // Gets the textbox.
            TextBox tb = GameState.Instance.InstanceTextBox;

            if (FindObjectOfType<SomeOtherGuy>().KnockedOut)
            {
                // Okay, here we go.
                tb.SetupTextBox( // Method for loading a new tree into our TextBox.
                    new Transition(
                        SpriteTransitionType.Overwrite,
                        this,
                        GameState.Instance.InstancePlayer,
                        new TextBoxContent(
                            new[]
                            {
                                "I'm disgusted by you."
                            }
                        )
                    ));
            }
            else if (FindObjectOfType<SomeOtherGuy>().WasPunched)
            {
                // Okay, here we go.
                tb.SetupTextBox( // Method for loading a new tree into our TextBox.
                    new Transition(
                        SpriteTransitionType.Overwrite,
                        this,
                        GameState.Instance.InstancePlayer,
                        new TextBoxContent(
                            new[]
                            {
                                "Was that really necessary?",
                                "Violence isn't always necessary to solve conflicts.",
                                "Next time, try being civil."
                            }
                        )
                    )
                );
                SaveState.ScriptState.Set(this, "was talked to", true);
                GameState.Instance
                         .AddQueuedCallback(
                             delegate
                             {
                                 GameState.Instance.InstancePlayer.Remark("I was just scolded by a table...");
                             });
            }
            else
            {
                GameState.Instance.InstancePlayer.Remark("What a nice table!");
                GameState.Instance.ClearAction();
            }
        }
    }
}